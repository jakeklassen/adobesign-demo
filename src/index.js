import passport from 'passport';
// EchoSign is now AdobeSign
import { Strategy as AdobeSignStrategy } from 'passport-echosign';
import https from 'https';
import express from 'express';
import daplie from 'localhost.daplie.com-certificates';

const port = process.env.PORT || 8443;
const app = express();

// Initialize passport
app.use(passport.initialize());

// Setup the AdobeSign strategy.
// Set `session` to false to avoid serialization errors. eg. We're not saving
// to the database right now.
passport.use(new AdobeSignStrategy({
	session: false,
	clientID: process.env.ADOBESIGN_CLIENT_ID,
	clientSecret: process.env.ADOBESIGN_CLIENT_SECRET,
	callbackURL: `https://localhost.daplie.com:${port}/auth/adobesign/callback`,
	scope: ['user_login:self', 'agreement_send:account', 'agreement_read']
}, (accessToken, refreshToken, profile, done) => {
	// Do real work here! Output for now.
	console.log(`access_token: ${accessToken}`);
	console.log(`refresh_token: ${refreshToken}`);
	console.log(profile);

	done(null, profile);
}));

// Show a simple signin button
app.get('/', (req, res) => {
	res.end('<a href="/auth/adobesign">Sign in<a/>');
});

// Initiate oauth process
app.get('/auth/adobesign', passport.authenticate('echosign'));

// Handle the adobesign callback.
app.get('/auth/adobesign/callback',
	passport.authenticate('echosign', { session: false, failureRedirect: '/login' }),
	(req, res) => {
		// Return relevant adobesign data.
		res.json({
			...req.query,
			message: 'Check your console for tokens'
		});
	});

// Create a TLS supported server.
const server = https.createServer(daplie, app);

server.listen(port, (err) => {
	if (err) {
		console.error(err);
	} else {
		console.log(`==> 🌎 listening on ${port}. Open up https://localhost.daplie.com:${port} in your browser.`);
	}
});
